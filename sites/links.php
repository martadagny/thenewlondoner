<style>
body{
    margin: 0px;
    background-image: url("./img/TheNewLondoner/AboutUs/back.png");
    background-size: cover;
    background-position: center;
    background-attachment: fixed;
}
h1{
    text-align: center;
    font-family: 'Arvo', serif;

}
.link{
    position:relative;
    width: 500px;
    height: 150px;
    top: 50px;
    margin: auto auto;
    margin-bottom: 5px;
    background-color: rgba(255,255,255,0.5);
}
.image{
    position:relative;
    width: 150px;
    height: 150px;
    background-color: rgba(255,255,255,0.5);
    float: left;
}

.about{
    position:relative;
    left: 5px;
    width: 340px;
    height: 140px;
    margin-top: 5px;
    //background-color: rgba(255,255,255,0.5);
    float: left;
    text-align: justify;
}
.title{
    color: #000;
}
.txt{
    padding-top: 5px;
    color: #000;
}


    </style>
<br><br><br><br>
    <h1>We are here to learn from each others</h1>

    <a href=https://blackculturalarchives.org/black-lives-matter>
    <div class=link>
        <div class=image> <img src=./img/TheNewLondoner/AboutUs/blm.png></div>
        <div class=about> 
            <div class=title> <a href=https://blackculturalarchives.org/black-lives-matter>Black Cultural Archives </a></div>
            <div class=txt> 2020 is the year when everything changes. The murder of George Floyd in America was the final injustice against people of African descent that the public could take. Centuries after the invention of racism it has become entrenched into every layer of Western society...</div>
        </div>
    </div>
    </a>

    <a href=https://www.inclusionlondon.org.uk/disability-in-london/useful-links/useful-links-for-disabled-londoners/>
    <div class=link>
        <div class=image>  <img src=./img/TheNewLondoner/AboutUs/dis.png></div>
        <div class=about>
            <div class=title><a href=https://www.inclusionlondon.org.uk/disability-in-london/useful-links/useful-links-for-disabled-londoners/
> Inclusion London</a></div>
        <div class=txt>Inclusion London supports over 70 Deaf and Disabled Organisations working across every London borough. Through these organisations, our reach extends to over 70,000 Disabled Londoners.</a>
         </div>
         </div>
    </div>
    </a>

    <a href=https://www.trustforlondon.org.uk/news/inequalities-and-disadvantage-london-focus-religion-and-belief/>
    <div class=link>
        <div class=image> <img src=./img/TheNewLondoner/AboutUs/rel.png></div>
        <div class=about> 
        <div class=title><a href=https://www.trustforlondon.org.uk/news/inequalities-and-disadvantage-london-focus-religion-and-belief/
> Trust for London</a></div>
        <div class=txt>In our comprehensive report on inequality and disadvantage in London published earlier this year, The Changing Anatomy of Economic Inequality in London (2007-2013), we provided a detailed picture of what happened to different population groups in London in the wake of the crisis and downturn... </div>
        </div>
    </div>
    </a>

    <a href=https://www.trustforlondon.org.uk/publications/held-back-single-parents-and-work-progression-london/>
    <div class=link>
        <div class=image> <img src=./img/TheNewLondoner/AboutUs/sp.png></div>
        <div class=about> 
        <div class=title><a href=https://www.trustforlondon.org.uk/publications/held-back-single-parents-and-work-progression-london/
> Trust for London</a></div>
        <div class=txt> Trust for London is an independent charitable foundation. We aim to tackle poverty and inequality in London and we do this by: funding voluntary and charity groups – currently we make grants totalling around £10 million a year and at any one time we are supporting up to 300 organisations...</div>
        </div>
    </div>
    </a>

    <a href=https://www.bbc.co.uk/news/55364865>
    <div class=link>
        <div class=image> <img src=./img/TheNewLondoner/AboutUs/hc.png></div>
        <div class=about> 
        <div class=title><a href=https://www.bbc.co.uk/news/55364865> BBC News</a></div>
        <div class=txt> The roll out of Covid-19 vaccines in the UK and US this week has led to a spate of new false claims about vaccines. We've looked into some of the most widely shared...</div>
        </div>
    </div>
    </a>



<a href=https://www.london.gov.uk/what-we-do/education-and-youth/teach-london/teacher-training-london>
    <div class=link>
        <div class=image> <img src=./img/TheNewLondoner/AboutUs/te.jpg></div>
        <div class=about> 
        <div class=title><a href=https://www.london.gov.uk/what-we-do/education-and-youth/teach-london/teacher-training-london> London Gov</a></div>
        <div class=txt> Explore your options if you have a degree (or are about to get one) and want to train to be a teacher in London. To find teacher training courses you can...</div>
        </div>
    </div>
    </a>
    <a href=https://www.citizensadvice.org.uk/law-and-courts/civil-rights/human-rights/what-are-human-rights/>
    <div class=link>
        <div class=image> <img src=./img/TheNewLondoner/AboutUs/hr.png></div>
        <div class=about> 
        <div class=title><a href=https://www.citizensadvice.org.uk/law-and-courts/civil-rights/human-rights/what-are-human-rights/> Citizens Advice</a></div>
        <div class=txt>Human rights are the basic rights and freedoms that belong to every person in the world. In the UK human rights are protected by the Human Rights Act 1998...</div>
        </div>
    </div>
    </a>
    <a href=https://www.womenintech.co.uk/women-in-tech-in-london>
    <div class=link>
        <div class=image> <img src=./img/TheNewLondoner/AboutUs/wit.png></div>
        <div class=about> 
        <div class=title><a href=https://www.womenintech.co.uk/women-in-tech-in-london> Women in Tech</a></div>
        <div class=txt></div>Around 19% of the UK tech sector is made up of women. It’s no secret that the tech industry has a significant gender gap. However, when it comes to the UK... </div>
        </div>
    </div>
    </a>
    <a href=https://www.forbes.com/sites/davidprosser/2017/01/06/10-london-entrepreneurs-under-30-to-watch-in-2017/>
    <div class=link>
        <div class=image> <img src=./img/TheNewLondoner/AboutUs/en.png></div>
        <div class=about> 
        <div class=title><a href=https://www.forbes.com/sites/davidprosser/2017/01/06/10-london-entrepreneurs-under-30-to-watch-in-2017/> Forbes</a></div>
        <div class=txt> Can the UK generate young entrepreneurs and innovators with the potential to take the world stage? A panel of leading figures in London’s start-up community believes it can...</div>
        </div>
    </div>
    </a>


