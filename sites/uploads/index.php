<meta http-equiv="pragma" content="no-cache" />
<?php

if (! empty($_POST["upload"])) {
    if (is_uploaded_file($_FILES['userImage']['tmp_name'])) {
        $targetPath = "./uploads/";
        $file = $_FILES['userImage']['name'];
        move_uploaded_file($_FILES['userImage']['tmp_name'], $targetPath.basename($file));
        if($_FILES['userImage']['type']=='image/jpeg'){
            $image = imagecreatefromjpeg($targetPath.basename($file));
        }
        if($_FILES['userImage']['type']=='image/png'){
            $image = imagecreatefrompng($targetPath.basename($file));
        }
        $im_width = imagesx($image);
        $im_height = imagesy($image);
        $template_height = 600*$im_height/$im_width;
        $template_image = imagecreatetruecolor(600, $template_height);
        imagecopyresampled($template_image, $image, 0, 0, 0, 0, 600, $template_height,$im_width,$im_height);
        imagejpeg($template_image, $targetPath.$_SESSION["user"][0].".jpg");
        $uploadedImagePath=  $targetPath.$_SESSION["user"][0].".jpg";
        //echo  $uploadedImagePath;
    }
}
?>
<html>
<head>
<link rel="stylesheet" href="./sites/upload/jquery.Jcrop.min.css" type="text/css" />
<script src="./sites/upload/jquery.min.js"></script>
<script src="./sites/upload/jquery.Jcrop.min.js"></script>
<style>


#cropped_img {
    margin-top: 40px;
}
</style>
</head>
<body>
<?php
$imagePath = "./img/noavatar.png";
if (! empty($uploadedImagePath)) {
    $imagePath = $uploadedImagePath;
}
?>
<div class=upload>
    <form name=myForm id=form class="post-form"  action="" method="post"
        enctype="multipart/form-data">
            <input name="userImage" id="userImage" type="file"
                class="inputFile" accept="image/*"><br> 
            <input id=submit type="submit"
                name="upload" value="Submit" class="btn btn-primary btn-block">



    <img src="<?php echo $imagePath; ?>" id="cropbox" class="img" /><br />
</div>
<div id="btn">
    <input type='button' id="crop" value='CROP'>
</div>
<div>
    <img src="#" id="cropped_img" style="display: none;">
</div>
<script type="text/javascript">


$(document).ready(function(){
    var size;
    $('#cropbox').Jcrop({
      aspectRatio: 1,
      onSelect: function(c){
       size = {x:c.x,y:c.y,w:c.w,h:c.h};
       $("#crop").css("visibility", "visible");     
      }
    });
 
    $("#crop").click(function(){
        var img = $("#cropbox").attr('src');
        //$("#cropped_img").show();
        $("#cropped_img").attr('src','./image-crop.php?x='+size.x+'&y='+size.y+'&w='+size.w+'&h='+size.h+'&img='+img);
        window.location = "./index.php?login";
    });
});
</script>
</body>
</html>