<?php
    ini_set('session.gc_maxlifetime', 900);
    session_set_cookie_params(900);

    session_start();
    if(isset($_GET["logout"])){
        session_destroy();
        session_start();
    }
?>
<head>
    <link rel="shortcut icon" type="image/png" href="./img/favicon.png"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="./style/main.css">
</head>
<body>
<?php 
    function isMobileDevice() { 
        return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]); 
    } 
    $pre = "";
    if(isMobileDevice()){ 
        $pre = "mobi_"; 
    }  
?> 
<?php

    $DBerror = 1;
    $sites = array("about"=>"links.php","upload"=>"upload/index.php","login"=>"login.php", "createUser"=>"createUser.php", "wall" => "wall.php", "who" => "who1.php","we" => "we1.php","are"=>"are1.php","why"=>"why1.php","contactus"=>"contact.php");
   
    include_once("./includes/db.php");
    include_once("./includes/user.php");
    include_once("./includes/forms.php");
    
    if(isset($_POST["loginU"])){
        logIn($_POST["login"],$_POST["pass"],$mysqli);
    }

    $home = 1;
    if($DBerror == 0){
        include("./includes/".$pre."menu.php");
        foreach($_GET as $k => $v){
            if(isset($_GET[$k])&&isset($sites[$k])){
                if($k != "upload")
                    include("./sites/" . $pre . $sites[$k]);
                else
                    include("./sites/" . $sites[$k]);
                $home = 0;
            }
        }

        if($home)
            include("./sites/" . $pre . "home.php");
        
    $mysqli->close();
    }
?>
</body>