<?php

function loginForm(){
?>
<div class="login-form">
    <form action="index.php?login" method="post">
        <h2 class="text-center">Log in</h2>       
        <div class="form-group">
            <input name=login type="text" class="form-control" placeholder="Username" required="required">
        </div>
        <div class="form-group">
            <input name=pass type="password" class="form-control" placeholder="Password" required="required">
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary btn-block">Log in</button>
        </div>
        <input type=hidden name=loginU valie=1>
    </form>
    <p class="text-center"><a href="index.php?createUser=1">Create an Account</a></p>
</div>
<?php
}

function createForm(){
    ?>
    <div class="login-form">
        <form action="index.php?createUser" method="post">
            <h2 class="text-center">Create an Account</h2>       
            <div class="form-group">
                <input name=login type="text" class="form-control" placeholder="Username" required="required">
            </div>
            <div class="form-group">
                <input name=pass1 type="password" class="form-control" placeholder="Password" required="required">
            </div>
            <div class="form-group">
                <input name=pass2 type="password" class="form-control" placeholder="Repeat password" required="required">
            </div>
            <div class="form-group">
                <input name=mail1 type="email" class="form-control" placeholder="E-mail" required="required">
            </div>
            <div class="form-group">
                <input name=mail2 type="email" class="form-control" placeholder="Repeat e-mail" required="required">
            </div>
            
            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-block">Create an Account</button>
            </div>
            <input type=hidden name=createU valie=1>
        </form>
        <p class="text-center"><a href="index.php?login=1">Log In</a></p>
    </div>
    <?php
    }
    

?>