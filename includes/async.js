function changeLikes (id){


    $.ajax({
        async:true,
        type: "POST",
        crossDomain: true,
        url: './includes/changeLikes.php',
        method: "GET",
        data: {"id":id}
    }).then(
        function(response)
        {
            loadLikes(id);
            
        },
        function()
        {
            if(id<0) id*=-1;
            var name = 'likes'+id;
            document.getElementById(name).innerHTML="No respnse";
        }
    );
}


function loadLikes (id){

    var settings = {
    "async": true,
    "crossDomain": true,
    "url": "./includes/loadLikes.php",
    "method": "GET",
    "data": {
        "id": id
    }
}

$.ajax(settings).done(function (response) {
   // var jsonData = JSON.parse(response);
   if(id<0) id*=-1;
   var name = 'likes'+id;
   document.getElementById(name).innerHTML=response;
});
}

function loadComments (id){

    var settings = {
    "async": true,
    "crossDomain": true,
    "url": "./includes/loadComments.php",
    "method": "GET",
    "data": {
        "id": id
    }
}

$.ajax(settings).done(function (response) {
   if(id<0) id*=-1;
   var name = 'comments'+id;
   document.getElementById(name).innerHTML=response;
});
}




function addComment (id){
    var frm = $('#add'+id);
    frm.submit(function (e) {

        e.preventDefault();
        $.ajax({
            async:true,
            crossDomain: true,
            url: './includes/addComment.php',
            method: "GET",
            data: {
                "id":id,
                "content":document.getElementById("content"+id).value
            }
        }).then(
            function(response)
            {
                loadComments(id);
            },
            function()
            {
                var name = 'comments'+id;
                document.getElementById(name).innerHTML="No respnse";
            }
        );
    });

    
}